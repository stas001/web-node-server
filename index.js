const express = require('express');
const bodyParser = require('body-parser');
const Article = require('./db').Article;
const read = require('node-readability');

const app = express();


app.set('port', process.env.PORT || 3000);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended:true }));


app.get('/articles', (req, res, next) => {
    Article.all((err, articles) => {
        if (err) throw new err;

        res.send(articles);
    });
});

app.post('/articles', (req, res, next) => {

    const url = req.body.url;

    read(url, (err, result) => {
        if (err || !result) {
            res.status(500)
                .send('Error downloading article');
        }

        const data = {
            title: result.title,
            content: result.content,
        };

        Article.create(data, (err, result) => {
            if (err) return next(err);

            res.send('OK');
        });
    });
});

app.get('/articles/:id', (req, res, next) => {
    const id = req.params.id;
    
    Article.find(id, (err, article) => {
        if (err) throw new err;
        res.send(article);
    });
});

app.put('/articles/:id', (req, res, next) => {
    const id = req.params.id-1;
    
    articles[id].title = req.body.title;

    res.send(articles[id]);
});

app.delete('/articles/:id', (req, res, next) => {
    const id = req.params.id;

    Article.delete(id, (err) => {
        if (err) throw new err;
        res.send({ message: 'Deleted.' });
    });
});


app.listen(app.get('port'), () => {
    console.log('Listen');
});

module.exports = app;